import { Component } from '@angular/core';
import { ProfileService } from './profile.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  assessment1Array: FormArray;
  title = 'check';

  constructor(
    private fb: FormBuilder,
    private profileService: ProfileService
  ) {}

  ngOnInit() {
    this.assessment1Array = this.fb.array([]);
    this.profileService.data.forEach((p) => {
      this.assessment1Array.push(this.createFormGroup(p));
    });
  }

  createFormGroup(data): FormGroup {
    return this.fb.group({
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
    });
  }

  createNewFormGroup(): FormGroup {
    return this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
    });
  }

  onAddData() {
    this.assessment1Array.push(this.createNewFormGroup());
  }
  onRemoveData(i) {
    this.assessment1Array.removeAt(i);
  }

  onSaveData() {

  }
}
