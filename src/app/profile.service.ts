import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  data = [
    {firstName: 'Nisar ahmad', lastName: 'Mughal', email:'07nisarahmad@gmail.com'}
  ]

  constructor() { }
}
